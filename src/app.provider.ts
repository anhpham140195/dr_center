import { Connection } from "typeorm";
import { Doctor } from "./entity/doctor.entity";
import { Patient } from "./entity/patient.entity";

export const appProvider = [

    {
        provide: 'DOCTOR',
        useFactory: (connection: Connection) => connection.getRepository(Doctor),
        inject: ['MySQL'],
    },
    {
        provide: 'PATIENT',
        useFactory: (connection: Connection) => connection.getRepository(Patient),
        inject: ['MySQL'],
    },
]