import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { appProvider } from './app.provider';

@Module({
  imports: [DatabaseModule],
  controllers: [AppController],
  providers: [AppService, ...appProvider],
})
export class AppModule { }
