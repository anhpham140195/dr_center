import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { AppModule } from './app.module';
declare const module: any;
import hbs = require('hbs');
// import exphbs = require('express-handlebars');
// import hbshelpers = require('handlebars-helpers');
// const multihelpers = hbshelpers();
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
  );

  // const helpersDM = {
  //   hlp: echo => `Echo: ${echo}.`,
  //   STATIC: `/static`,
  // };

  app.useStaticAssets(join(__dirname, '../assets'));
  app.setBaseViewsDir(join(__dirname, '..', 'views'));
  app.setViewEngine('hbs');

  hbs.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

  hbs.registerPartials(join(__dirname, '../views', 'partials'));
  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
  await app.listen(3000);
}
bootstrap();