import { Get, Controller, Res, Render } from '@nestjs/common';
import { Response } from 'express';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private appService: AppService) { }



  @Get('/doctors')
  @Render('doctors')
  async doctors() {
    const list = await this.appService.doctors()
    return { doctors: list, title: "DOCTOR" };
  }

  @Get('/home')
  @Render('home')
  root() {
    return this.appService.sideBar()

  }
  @Get('/patients')
  @Render('patients')
  async patients() {
    const list = await this.appService.patients()
    return { title: "PATIENT", patients: list }
  }
}
