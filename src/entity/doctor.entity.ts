import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"


@Entity('doctor')
export class Doctor {
    @PrimaryGeneratedColumn()
    id: number
    @Column()
    name: string

    @Column()
    avatar: string

    @Column()
    phone: string
    @Column()
    address: string
    @Column()
    email: string;
    @Column({ name: 'hospital' })
    hospital: string

    @Column({ name: 'deleted' })
    deleted: boolean

    @Column('timestamp', { precision: 6, name: 'created_at', default: () => 'CURRENT_TIMESTAMP(6)' })
    createdAt: Date;

    @Column('timestamp', { precision: 6, name: 'updated_at', default: () => 'CURRENT_TIMESTAMP(6)' })
    updateAt: Date;

}
