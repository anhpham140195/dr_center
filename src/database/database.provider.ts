import { createConnection } from 'typeorm';

export const databaseProviders = [
    {
        provide: 'MySQL',
        useFactory: async () => await createConnection({
            type: 'mysql',
            host: 'localhost',
            port: 3307,
            username: 'root',
            password: '123',
            database: 'hospital',
            entities: [__dirname + '/../**/*.entity{.ts,.js}'],
            synchronize: true,
        }),
    }

];