import { Injectable, Inject } from '@nestjs/common';
import { Doctor } from './entity/doctor.entity';
import { Repository } from 'typeorm';
import { Patient } from './entity/patient.entity';

@Injectable()
export class AppService {
  constructor(
    @Inject('DOCTOR')
    private readonly doctorModel: Repository<Doctor>,
    @Inject('PATIENT')
    private readonly patientModel: Repository<Patient>,
  ) { }

  sideBar() {
    return { title: 'HOME' }
  }

  async doctors() {
    return await this.doctorModel.find({ deleted: false })

  }

  async patients() {
    return await this.patientModel.find({ deleted: false })
  }
}
